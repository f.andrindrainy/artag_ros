#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import cv2
import numpy as np
from sys import argv
from copy import deepcopy
import matplotlib.pyplot as plt
import matplotlib as mpl
import rospkg
import rospy
import os
import std_msgs
from geometry_msgs.msg import PoseStamped
import time
rospack = rospkg.RosPack()

tag_path = os.path.join(rospack.get_path('ar_tag_pkg'),'tags_test')
# Save image in set directory
# Read RGB image
img = cv2.imread(tag_path+'/'+'ar_tag_3.JPG')
# img = cv2.imread(tag_path+'/'+'ar_tag_2.JPG')
# img = cv2.imread(tag_path+'/'+'ar_tag_1.JPG')
# img = cv2.imread(tag_path+'/'+'ARtag1.pdf')
 
# Output img with window name as 'image'
# cv2.imshow('image', img)
 
# # Maintain output window utill
# # user presses a key
# cv2.waitKey(0)       
 
# # Destroying present windows on screen
# cv2.destroyAllWindows()

arucoDict = cv2.aruco.Dictionary_get(cv2.aruco.DICT_APRILTAG_36h11)
arucoParams = cv2.aruco.DetectorParameters_create()
(corners, ids, rejected) = cv2.aruco.detectMarkers(img, arucoDict,
	parameters=arucoParams)

print(ids)

m_client_pub= rospy.Publisher("/move_base_simple/goal",PoseStamped, queue_size=10)
goal = PoseStamped()
rospy.init_node("ar_tag_pkg")
goal.header.frame_id = "map"
# print(rospy.Time.now())
time.sleep(1)
goal.header.stamp= rospy.Time.now()
if ids == [[22]] :
    goal.pose.position.x= -2
    goal.pose.position.y= 0.01
    goal.pose.position.z= 0
    
    goal.pose.orientation.x=0
    goal.pose.orientation.y=0
    goal.pose.orientation.z=0
    goal.pose.orientation.w=1
    
    m_client_pub.publish(goal)
    print("ar_tag_3")
if ids == [[21]] :
    goal.pose.position.x= 0
    goal.pose.position.y= 2
    goal.pose.position.z= 0
    
    goal.pose.orientation.x=0
    goal.pose.orientation.y=0
    goal.pose.orientation.z=0
    goal.pose.orientation.w=1
    
    m_client_pub.publish(goal)
    print("ar_tag_2")
    
elif ids == [[20]] :
    goal.pose.position.x= 2
    goal.pose.position.y= 0.01
    goal.pose.position.z= 0
    
    goal.pose.orientation.x=0
    goal.pose.orientation.y=0
    goal.pose.orientation.z=0
    goal.pose.orientation.w=1
    
    m_client_pub.publish(goal)
    print("ar_tag_1")
