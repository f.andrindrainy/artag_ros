#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import cv2
import numpy as np
from sys import argv
from copy import deepcopy
import matplotlib.pyplot as plt
import matplotlib as mpl
import rospkg
import rospy
import os
import std_msgs
from geometry_msgs.msg import PoseStamped
import time

from math import *
import numpy as np
from scipy.spatial.transform import Rotation as R

def euler_from_quaternion(x, y, z, w):
    """
    Convert a quaternion into euler angles (roll, pitch, yaw)
    roll is rotation around x in radians (counterclockwise)
    pitch is rotation around y in radians (counterclockwise)
    yaw is rotation around z in radians (counterclockwise)
    """
    t0 = +2.0 * (w * x + y * z)
    t1 = +1.0 - 2.0 * (x * x + y * y)
    roll_x = atan2(t0, t1)
        
    t2 = +2.0 * (w * y - z * x)
    t2 = +1.0 if t2 > +1.0 else t2
    t2 = -1.0 if t2 < -1.0 else t2
    pitch_y = asin(t2)
        
    t3 = +2.0 * (w * z + x * y)
    t4 = +1.0 - 2.0 * (y * y + z * z)
    yaw_z = atan2(t3, t4)
        
    return roll_x, pitch_y, yaw_z # in radians

#### Fonction pour ajuster la rotation du robot selon la rotation de l'image ######

#Détection du chemin d'accès au fichier image AprilTag
rospack = rospkg.RosPack()

tag_path = os.path.join(rospack.get_path('ar_tag_pkg'),'tags_test')

#Lecture du fichier et stockage dans une variable global 

img = cv2.imread(tag_path+'/'+'ar_tag_3.JPG')
# img = cv2.imread(tag_path+'/'+'ar_tag_2.JPG')
# img = cv2.imread(tag_path+'/'+'ar_tag_1.JPG')
# img = cv2.imread(tag_path+'/'+'ARtag1.pdf')

#Definition des paramètres de fonction : Dictionnaire et paramètre de détection  
arucoDict = cv2.aruco.Dictionary_get(cv2.aruco.DICT_APRILTAG_36h11)
arucoParams = cv2.aruco.DetectorParameters_create()

#Fonction de détection du AprilTag
(corners, ids, rejected) = cv2.aruco.detectMarkers(img, arucoDict,
	parameters=arucoParams)

#Affichage de la lecture
print(ids)

#Paramétrage du publisher ROS et ses messages
m_client_pub= rospy.Publisher("/move_base_simple/goal",PoseStamped, queue_size=10)
goal = PoseStamped()
rospy.init_node("ar_tag_pkg")
goal.header.frame_id = "map"
# print(rospy.Time.now())
time.sleep(1)
goal.header.stamp= rospy.Time.now()

#imgaruco,px,py,pz,roll,pitch,yaw =arucoco(img)

#Paramètre de détection de l'orientation : matrice de calibration, distorsion de la caméra, paramètre important pour la détection

matrix_coefficients = np.array(
 [[146.9752954  ,  0.    ,     328.50001114],
 [  0.   ,      138.01316896 ,245.5000003 ],
 [  0.       ,    0.      ,     1.        ]]
    )
distortion_coefficients = np.array(
      [[-1.59247305e-02,  5.39979029e-05 ,-6.85144417e-04 ,-1.08721992e-03,
  -4.95197590e-08]]
    )
distortion_coefficients= np.dot(  distortion_coefficients, 0)

#Estimation de la position
rvec, tvec ,a= cv2.aruco.estimatePoseSingleMarkers(corners,0.05, matrix_coefficients, distortion_coefficients)

#Traitement des résultats
rotation_matrix = np.eye(4)
rotation_matrix[0:3, 0:3] = cv2.Rodrigues(np.array(rvec[0][0]))[0]
r = R.from_matrix(rotation_matrix[0:3, 0:3])
r=R.from_matrix(rotation_matrix[0:3, 0:3])
quat=r.as_quat()
#quaternion en absolue
print(quat)

roll,pitch,yaw = euler_from_quaternion(quat[0],quat[1],quat[2],quat[3])
yaw=degrees(yaw)
r = R.from_euler('z',yaw,degrees=True)
quat=r.as_quat()
#quaternion en rotation par rapport à Z
print(quat)

#Publication des résultats
if ids == [[22]] :
    goal.pose.position.x= -2
    goal.pose.position.y= 0.01
    goal.pose.position.z= 0
    

    print("ar_tag_3")
if ids == [[21]] :
    goal.pose.position.x= 0
    goal.pose.position.y= 2
    goal.pose.position.z= 0
    
    print("ar_tag_2")
    
elif ids == [[20]] :
    goal.pose.position.x= 2
    goal.pose.position.y= 0.01
    goal.pose.position.z= 0
    
    print("ar_tag_1")

goal.pose.orientation.x=quat[0]
goal.pose.orientation.y=quat[1]
goal.pose.orientation.z=quat[2]
goal.pose.orientation.w=quat[3]
# goal.pose.orientation.w=1

m_client_pub.publish(goal)