

# Rapport de test de recrutement iFollow

### Nom : ANDRINDRAINY Fiderana
### email : f.andrindrainy@gmail.com
### tel : 0760439944

### 1. Mise en place de l'environnement de test

les objectifs dans cette partie sont :
* Installation de l'environnement de simulation Turtlebot 3 dans Gazebo
* Visualisation du robot et d'une map dans RVIZ
* Installation d'une stack de localization et de navigation 
* Contrôle du robot en téléopération 
* Contrôle du robot avec un consigne de position

#### 1.1 Installation de l'environnement de simulation Turtlebot3

Dans cette partie, nous utiliserons comme source ce [lien](https://emanual.robotis.com/docs/en/platform/turtlebot3/quick-start/#pc-setup)

De ce fait, la première étape sera l'installation de UBUNTU dans le PC.
Dans notre cas, où il est déjà installer, nous devrons alors taper le commande suivant sur un terminal pour vérifier la bonne version de UBUNTU

    ~$ lsb_release -a

Ainsi, nous avons le résultat suivant

        No LSB modules are available.
        Distributor ID:	Ubuntu
        Description:	Ubuntu 20.04.4 LTS
        Release:	20.04
        Codename:	focal


Il faudra faire de même avec l'installation de ROS. 
Notre version sera alors ROS-Noetic 

~$ rosversion  -d
noetic

Dans le cas où il faudra installer ROS, on suivra le démarche décrie sur ce [site](http://wiki.ros.org/noetic/Installation/Ubuntu)

Maintenant, il faudra installer toute les dépendances de ROS et celle du TURTLEBOT3

$ sudo apt-get install ros-noetic-joy ros-noetic-teleop-twist-joy   ros-noetic-teleop-twist-keyboard ros-noetic-laser-proc   ros-noetic-rgbd-launch ros-noetic-depthimage-to-laserscan   ros-noetic-rosserial-arduino ros-noetic-rosserial-python   ros-noetic-rosserial-server ros-noetic-rosserial-client   ros-noetic-rosserial-msgs ros-noetic-amcl ros-noetic-map-server   ros-noetic-move-base ros-noetic-urdf ros-noetic-xacro   ros-noetic-compressed-image-transport ros-noetic-rqt*   ros-noetic-gmapping ros-noetic-navigation ros-noetic-interactive-markers ros-noetic-costmap-2d


$ sudo apt-get install ros-noetic-dynamixel-sdk
$ sudo apt-get install ros-noetic-turtlebot3-msgs
$ sudo apt-get install ros-noetic-turtlebot3

A présent, nous allons paramétrer notre espace de travail : 

mkdir ~/Turtlebot3
cd ~/Turtlebot3
mkdir src
cd src

Désormais, il est possible de copier le projet de git

$ git clone -b noetic-devel https://github.com/ROBOTIS-GIT/turtlebot3_simulations.git

Il sera alors possible de compiler notre projet et de configurer notre terminal avec la commande suivante:

$ cd ~/Turtlebot3
$ catkin_make
$ source devel/setup.bash 
$ export TURTLEBOT3_MODEL=burger

Ainsi, avec ces configurations, nous pourrons lancer la simulation

$ roslaunch turtlebot3_gazebo turtlebot3_empty_world.launch 




Précédemment, la simulation s'est fait sur une environnement vide. Pour la suite, on va choisir de faire une simulation avec l'environnement "world" avec la commande suivante

$ roslaunch turtlebot3_gazebo turtlebot3_world.launch 




Maintenant, pour la construction de la carte, on utilisera le module "slam" qui sera appelé grâce au commande suivante (dans une nouvelle fenêtre de console):

$ cd ~/Turtlebot3
$ source devel/setup.bash 
$ export TURTLEBOT3_MODEL=burger
$ roslaunch turtlebot3_slam turtlebot3_slam.launch 



De même, pour la teleopération sur clavier, et la navigation du robot dans son environnement, il faudra ouvrir une nouvelle fenetre de terminal et suivre la commande suivante

$ cd ~/Turtlebot3
$ source devel/setup.bash 
$ export TURTLEBOT3_MODEL=burger
$ roslaunch turtlebot3_teleop turtlebot3_teleop_key.launch 



Après la construction de la carte, on pourra alors le sauvegarder avec la commande suivante :

$ rosrun map_server map_saver -f ~/map



Maintenant, il pourra être possible de naviger dans ce monde en faisant la commande suivante

$ roslaunch turtlebot3_navigation turtlebot3_navigation.launch map_file:=$HOME/map.yaml



En initalisant la position avec " 2D Pose Estimate", et en téléopérant le robot, il est possible d'affiner la position réel du robot (position en flèche vert)


Ainsi, en activant la costmap, nous avons le résultat suivant

Il est alors possible de mouvoir le robot en lui donnant une position "2D Nav Goal"




### 2. Multiplexeur de commande

La fonction de multiplexeur sera alors effectué à l'aide de sa commande suivante: 

$ rosrun topic_tools mux sortie_mux /cmd_local /cmd_web mux:=mux_cmdvel

Pour pouvoir selectionner (switcher de source) l'entrée, il faut appeler le service correspondant : 

$ rosservice call /mux_cmdvel/select "topic: 'cmd_local'" 

On pourra visualiser le projet via rqt


### 3. Téléopération à distance

Ce module est développé sous une autre repository, 
https://gitlab.com/f.andrindrainy/mqtt_ros.git

Il effectue le subscribe et le publisher en MQTT, et il est fonctionnelle

### 4. Envoi de Goal Déterminé par un tag visuel

Ce module a été développer sur Gitlab, et il est terminé . 
Veillez le trouver sur le [lien](https://gitlab.com/f.andrindrainy/artag_ros.git)

Cependant, avant de lancer le programme, il sera nécessaire de refaire les commandes de base après la clone de git

$ cd ~/Turtlebot3
$ source devel/setup.bash 
$ export TURTLEBOT3_MODEL=burger



### 5. Utilisation des caractéristiques visuelles

Dépendance : 
$ sudo apt-get install python3-scipy

### 6. Utilisation d'une capture d'Image

Dépendance : sudo apt-get install ros-noetic-cv-bridge 

Dans cette partie, nous allons utiliser le lien suivant pour un noeud driver du webcam

https://github.com/ros-drivers/video_stream_opencv.git

De ce fait, afin de l'installer, il faudra faire le cloner dans le fichier source de votre workspace.

afin de tester ce noeud, on utilisera la commande suivante :
$ cd ~/Turtlebot3
$ catkin_make
$ source devel/setup.bash

$ roslaunch video_stream_opencv webcam.launch 

Pour afficher, dans une autre console, on fera :
$ rqt_image_view 
puis, selectionner le topic d'image "/webcam/image_raw"

Ainsi, avec le script Q6, il est possible d'avoir les résultats dans le fichier "Q6_Valisation_AprilTag"

Veillez toujours verifier si la console est bien paramétrer ($ source devel/setup.bash) avant de lancer les scripts pythons